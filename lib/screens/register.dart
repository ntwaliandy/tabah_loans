import 'dart:convert';

import "package:flutter/material.dart";
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:tabah_loans/screens/login.dart';
import 'package:tabah_loans/screens/otp.dart';

import '../components/api.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({super.key});

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final _formKey = GlobalKey<FormState>();
  String _email = "";
  String _password = "";
  String _message = "";
  bool isLoading = false;

  regSubmit() async {
    setState(() {
      isLoading = true;
      _message = "";
    });
    final url = Uri.parse("${apiUrl}user-client/register_user");
    final bodyData = {
      "email": _email,
      "password": _password,
    };
    final headers = {
      "Content-Type": "application/json",
      "Accept": "application/json"
    };
    Response response =
        await http.post(url, headers: headers, body: jsonEncode(bodyData));

    // decoding the response
    var result = jsonDecode(response.body);
    setState(() {
      isLoading = false;
    });
    if (result['status'] == 100) {
      // ignore: use_build_context_synchronously
      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context) => OtpScreen(email: _email)));
    } else {
      setState(() {
        _message = result['message'];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    double screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              // logo
              Container(
                margin: EdgeInsets.only(top: screenHeight * 0.1),
                child: Center(
                  child: Image.asset(
                    "assets/images/logo.jpg",
                    width: screenWidth * 0.6,
                    height: screenHeight * 0.3,
                  ),
                ),
              ),
              // spacer
              SizedBox(height: screenHeight * 0.03),
              // heading
              Text(
                "Register with Tabah Quick Loan",
                style: TextStyle(
                  fontSize: screenWidth * 0.05,
                  letterSpacing: screenWidth * 0.005,
                  fontWeight: FontWeight.bold,
                ),
              ),
              // spacer
              SizedBox(height: screenHeight * 0.04),
              // email address
              Container(
                margin: EdgeInsets.symmetric(horizontal: screenWidth * 0.03),
                child: TextFormField(
                  decoration: InputDecoration(
                    icon: const Icon(Icons.email),
                    hintText: "Enter your email",
                    labelText: "Email",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(screenWidth * 0.02),
                    ),
                  ),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter a valid email address';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    setState(() {
                      _email = value;
                    });
                  },
                ),
              ),
              // spacer
              SizedBox(height: screenHeight * 0.03),
              // password
              Container(
                margin: EdgeInsets.symmetric(horizontal: screenWidth * 0.03),
                child: TextFormField(
                  obscureText: true,
                  decoration: InputDecoration(
                    icon: const Icon(Icons.password),
                    hintText: "Enter your password",
                    labelText: "Password",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(screenWidth * 0.02),
                    ),
                  ),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter a valid password';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    setState(() {
                      _password = value;
                    });
                  },
                ),
              ),
              // spacer
              SizedBox(height: screenHeight * 0.03),
              // error
              Text(
                _message,
                style: TextStyle(
                    color: Colors.red,
                    fontWeight: FontWeight.bold,
                    fontSize: screenHeight * 0.02,
                    letterSpacing: screenHeight * 0.005),
              ),
              // register button
              Container(
                color: Colors.blue,
                width: screenWidth * 0.95,
                child: ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      regSubmit();
                    }
                  },
                  child: Padding(
                    padding:
                        EdgeInsets.symmetric(vertical: screenHeight * 0.015),
                    child: isLoading
                        ? const CircularProgressIndicator(color: Colors.white)
                        : Text(
                            "REGISTER",
                            style: TextStyle(
                              fontSize: screenHeight * 0.025,
                              color: Colors.white,
                            ),
                          ),
                  ),
                ),
              ),

              // spacer
              SizedBox(height: screenHeight * 0.03),
              GestureDetector(
                onTap: () {
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const LoginScreen()));
                },
                child: const Text("Already have an Account?, Click Here!"),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
