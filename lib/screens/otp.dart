import 'dart:convert';

import "package:flutter/material.dart";
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:tabah_loans/screens/home.dart';

import '../components/api.dart';

class OtpScreen extends StatefulWidget {
  final String email;
  const OtpScreen({super.key, required this.email});

  @override
  State<OtpScreen> createState() => _OtpScreenState();
}

class _OtpScreenState extends State<OtpScreen> {
  final _formKey = GlobalKey<FormState>();
  String _otp = "";
  String _message = "";
  bool isLoading = false;

  otpSubmit() async {
    setState(() {
      isLoading = true;
      _message = "";
    });
    final url = Uri.parse("${apiUrl}user-client/verify_otp");
    final bodyData = {
      "email": widget.email,
      "otp": _otp,
    };
    final headers = {
      "Content-Type": "application/json",
      "Accept": "application/json"
    };
    Response response =
        await http.post(url, headers: headers, body: jsonEncode(bodyData));

    // decoding the response
    var result = jsonDecode(response.body);
    setState(() {
      isLoading = false;
    });
    if (result['status'] == 100) {
      // ignore: use_build_context_synchronously
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => HomeScreen(email: widget.email)));
    } else {
      setState(() {
        _message = result['message'];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    double screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              // logo
              Container(
                margin: EdgeInsets.only(top: screenHeight * 0.1),
                child: Center(
                  child: Image.asset(
                    "assets/images/logo.jpg",
                    width: screenWidth * 0.6,
                    height: screenHeight * 0.3,
                  ),
                ),
              ),
              // spacer
              SizedBox(height: screenHeight * 0.03),
              // heading
              Text(
                "Confirm OTP sent to email starting with ${widget.email.substring(0, 5)}...",
                style: TextStyle(
                  fontSize: screenWidth * 0.035,
                  letterSpacing: screenWidth * 0.005,
                  fontWeight: FontWeight.bold,
                ),
              ),
              // spacer
              SizedBox(height: screenHeight * 0.04),
              // otp
              Container(
                margin: EdgeInsets.symmetric(horizontal: screenWidth * 0.03),
                child: TextFormField(
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    icon: const Icon(Icons.confirmation_num_outlined),
                    hintText: "Enter your OTP",
                    labelText: "OTP",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(screenWidth * 0.02),
                    ),
                  ),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter a valid OTP';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    setState(() {
                      _otp = value;
                    });
                  },
                ),
              ),
              // spacer
              SizedBox(height: screenHeight * 0.03),
              Text(
                _message,
                style: TextStyle(
                    color: Colors.red,
                    fontWeight: FontWeight.bold,
                    fontSize: screenHeight * 0.02,
                    letterSpacing: screenHeight * 0.005),
              ),
              // login button
              Container(
                color: Colors.blue,
                width: screenWidth * 0.95,
                child: ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      otpSubmit();
                    }
                  },
                  child: Padding(
                    padding:
                        EdgeInsets.symmetric(vertical: screenHeight * 0.015),
                    child: isLoading
                        ? const CircularProgressIndicator(color: Colors.white)
                        : Text(
                            "SUBMIT",
                            style: TextStyle(
                              fontSize: screenHeight * 0.025,
                              color: Colors.white,
                            ),
                          ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
